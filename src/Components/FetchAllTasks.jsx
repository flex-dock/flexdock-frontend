import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { apiConfig } from '../Constants/ApiConfig'

// Helper function to extract the desired part of the task definition name and capitalize the first letter
const getShortTaskDefName = (taskDefName) => {
    const parts = taskDefName.split('-')
    let shortName
    if (parts.length > 3) {
        shortName = `${parts[0]}-${parts[1]}`
    } else {
        shortName = parts[0]
    }
    return shortName.charAt(0).toUpperCase() + shortName.slice(1)
}

// TaskCard component to display individual task details
const TaskCard = ({ task }) => {

    const navigate = useNavigate()

    const launchDock = async (id) => {
        const dockResponse = await axios.post(`${apiConfig.flexDock}/launchdock`, {}, {
            headers: {
                token: "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ2b3J0ZXgxNjQ5QGdtYWlsLmNvbSJ9.W_2C3IKUZU-5q-iqskVoQSiQFsT7R9Sr_VtvN8u42dA",
                taskDefId: id
            }
        })

        const data = await dockResponse.data
        navigate(`/dockinfo/${data.dockId}`)
    }

    return (
        <div className="bg-blue-50 shadow-lg rounded-lg p-6 m-4 w-60 transform transition duration-500 hover:scale-105">
            <h3 className="text-xl font-bold text-blue-700 mb-4">{getShortTaskDefName(task.taskDefName)}</h3>
            <p className="text-gray-700 mb-2"><strong>Task ID:</strong> {task.taskDefId}</p>
            <p className="text-gray-700 mb-4"><strong>Revision:</strong> {task.revision}</p>
            <button className="bg-blue-500 text-white px-4 py-2 rounded hover:bg-blue-700 transition duration-300"
                onClick={() => launchDock(task.taskDefId)}
            >
                Launch Dock
            </button>
        </div>
    )
}

// FetchAllTasks component to fetch and display tasks
const FetchAllTasks = () => {
    const [tasks, setTasks] = useState([])

    useEffect(() => {
        const fetchAllTasks = async () => {
            try {
                const response = await axios(`${apiConfig.flexDock}/fetchalltaskdefs`)
                const data = response.data
                setTasks(data)
            } catch (error) {
                console.error('Error fetching tasks:', error)
            }
        }

        fetchAllTasks()
    }, [])

    return (
        <div className="min-h-screen bg-gradient-to-br from-blue-100 to-blue-300 p-8">
            <h1 className="text-4xl font-extrabold text-center text-blue-800 my-8">Docks available</h1>
            <div className="flex flex-wrap justify-center">
                {tasks.map((task) => (
                    <TaskCard key={task.taskDefId} task={task} />
                ))}
            </div>
        </div>
    )
}

export default FetchAllTasks
