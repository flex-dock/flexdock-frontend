import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import '../Styles/DockMainPage.css';
import { apiConfig } from '../Constants/ApiConfig'

// Helper function to get the color for a state
const getStateColor = (state) => {
    switch (state) {
        case 'PROVISIONING':
            return 'bg-yellow-500';
        case 'PENDING':
            return 'bg-orange-300';
        case 'ACTIVATING':
            return 'bg-blue-500';
        case 'RUNNING':
            return 'bg-green-500';
        case 'DEACTIVATING':
            return 'bg-orange-500';
        case 'STOPPING':
            return 'bg-red-500';
        case 'DEPROVISIONING':
            return 'bg-purple-500';
        case 'STOPPED':
            return 'bg-gray-500';
        case 'DELETED':
            return 'bg-red';
        default:
            return 'bg-gray-200';
    }
};

// States that should have blinking animation
const blinkingStates = ['PROVISIONING', 'PENDING', 'ACTIVATING', 'RUNNING', 'DEACTIVATING', 'STOPPING', 'DEPROVISIONING'];

const DockMainPage = () => {
    const { id } = useParams();
    const [dockInfo, setDockInfo] = useState(null);
    const [isDeleting, setIsDeleting] = useState(false);

    useEffect(() => {
        let interval;

        const fetchDockInfo = async () => {
            try {
                const dockInfo = await axios.get(`${apiConfig.flexDock}/getdockinfo`, {
                    headers: {
                        dockId: id
                    }
                });
                setDockInfo(dockInfo.data);
                return dockInfo.data.dockState;

            } catch (error) {
                console.error('Error fetching dock info:', error);
            }
        };

        const startFetching = () => {
            interval = setInterval(async () => {
                const state = await fetchDockInfo();
                if (state === 'DELETED') {
                    clearInterval(interval);
                }
            }, 5000);
        };
        
        startFetching();
        setIsDeleting(false);

        return () => clearInterval(interval);

    }, [id, isDeleting]);

    const deleteDock = async (id) => {
        try {
            const deleteDock = await axios.delete(`${apiConfig.flexDock}/deletedock`, {
                headers: {
                    dockId: id
                }
            });
            const deleteResponse = await deleteDock.data;
            
            // Start fetching after a delay of 3 seconds
            setTimeout(() => {
                setIsDeleting(true);
            }, 3000);

        } catch (error) {
            console.error('Error deleting dock:', error);
            setIsDeleting(false);
        }
    };

    return (
        <div className="min-h-screen bg-gradient-to-br from-blue-100 to-blue-300 p-8">
            <h1 className="text-4xl font-extrabold text-center text-blue-800 my-8">Dock Information</h1>
            {dockInfo ? (
                <div className="max-w-4xl mx-auto bg-white shadow-lg rounded-lg p-6 transform transition duration-500 hover:scale-105">
                    <div className="flex flex-wrap justify-around">
                        <div className="w-full md:w-1/2 lg:w-1/3 p-2">
                            <h2 className="text-xl font-bold text-blue-700 mb-2">Dock ID</h2>
                            <p className="text-gray-700">{dockInfo.dockId}</p>
                        </div>
                        <div className="w-full md:w-1/2 lg:w-1/3 p-2">
                            <h2 className="text-xl font-bold text-blue-700 mb-2">Task Definition ID</h2>
                            <p className="text-gray-700">{dockInfo.taskDefId}</p>
                        </div>
                        <div className="w-full md:w-1/2 lg:w-1/3 p-2">
                            <h2 className="text-xl font-bold text-blue-700 mb-2">Dock IP</h2>
                            <a href={dockInfo.dockIP} target='_blank' className="text-blue-700">{dockInfo.dockIP}</a>
                        </div>
                        <div className="w-full md:w-1/2 lg:w-1/3 p-2 flex items-center">
                            <h2 className="text-xl font-bold text-blue-700 mb-2">Dock State</h2>
                            <span className={`w-4 h-4 rounded-full ml-2 ${getStateColor(dockInfo.dockState)} ${blinkingStates.includes(dockInfo.dockState) ? 'blink' : ''}`}></span>
                            <p className="text-gray-700 ml-2">{dockInfo.dockState}</p>
                        </div>
                        <div className="w-full md:w-1/2 lg:w-1/3 p-2">
                            <h2 className="text-xl font-bold text-blue-700 mb-2">Created At</h2>
                            <p className="text-gray-700">{new Date(dockInfo.createdAt).toLocaleString()}</p>
                        </div>
                        <div className="w-full md:w-1/2 lg:w-1/3 p-2">
                            <h2 className="text-xl font-bold text-blue-700 mb-2">Deleted At</h2>
                            <p className="text-gray-700">{dockInfo.deletedAt===null?'-':new Date(dockInfo.deletedAt).toLocaleString()}</p>
                        </div>
                    </div>
                    <button
                        className="bg-red-500 text-white px-4 py-2 rounded mt-4 hover:bg-red-700 transition duration-300"
                        onClick={() => deleteDock(dockInfo.dockId)}
                        disabled={dockInfo.dockState==="DELETED"?true:false}
                    >
                        {dockInfo.dockState==="DELETED" ? 'Dock has been deleted' : 'Delete Dock'}
                    </button>
                </div>
            ) : (
                <p className="text-center text-blue-800">Loading...</p>
            )}
        </div>
    );
};

export default DockMainPage;
