import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import FetchAllTasks from "./Components/FetchAllTasks";
import DockMainPage from "./Components/DockMainPage";

function App() {
  return (
    <>
      <Router>
        <Routes>
          <Route exact path="/" element={<FetchAllTasks />} />
          <Route exact path="/dockinfo/:id" element={<DockMainPage />} />
        </Routes>
      </Router>
    </>
  );
}

export default App;
